import {CommandInterface, OptionsDefinition} from '../command.interface';
import {BaseCommand} from '../base-command';
import * as path from 'path';
import * as fs from 'fs';
import * as yaml from 'js-yaml';
import * as prompt from 'prompt';
import {InitCloneRepo} from './init-clone-repo';
import {Me} from '../me/me';
import {Main} from '../main';
import {GitRepoInterface, PlagiarizeMeYamlConfigInterface} from '../plagiarize-me-yaml-config.interface';
import {Logger} from '../logger';
import {Git} from 'git-interface';
import {StringUtil} from '../util/string.util';

export class Init implements CommandInterface {

    public static readonly CONFIG_FILE = 'plagiarize.yaml';
    protected readonly baseCommand: BaseCommand;
    private _repo: { url: string };

    public constructor(protected readonly logger: Logger) {
        this.baseCommand = new BaseCommand(logger, this);
    }

    public initializeOptions(optionsDefinitions: OptionsDefinition[] = []): Record<string, any> {
        return this.baseCommand.initializeOptions(optionsDefinitions.concat([
            {name: 'source-directory', alias: 's', type: String, description: `The source directory to copy which also contains ${Me.CONFIG_FILE}`},
        ]));
    }

    public setCWD(cwd: string) {
        this.baseCommand.setCWD(cwd);
    }

    public async run() {
        this.baseCommand.run();

        this._validatePlagiarizeNotExists();
        await this._ensureWorkingDirectoryIsGitRepo();

        const data     = await this._getYaml();
        const safeData = yaml.safeDump(data);
        const output   = safeData;

        const filePath = `${this.baseCommand.cwd()}/${Init.CONFIG_FILE}`;
        fs.writeFileSync(filePath, output, 'utf-8');

        this.logger.log(`Created config file ${filePath}`);
    }

    private async _getYaml(): Promise<any> {
        const sourceRepo = await this._getRepo();

        if(!this.baseCommand.options.autoconfigure){
            this.logger.log('Please configure the following properties in order to plagiarize the source repository:');
        }

        const sourceStringConfig = await this.getSourceConfig(sourceRepo, 'strings');
        const replaceConfig      = await this._getStringConfig(sourceStringConfig);

        const sourceVarsConfig = await this.getSourceConfig(sourceRepo, 'vars');
        const varsConfig       = await this._getStringConfig(sourceVarsConfig);

        let result: any = {
            repo:    sourceRepo,
            strings: replaceConfig,
        }
        if(varsConfig && Object.keys(varsConfig).length > 0){
            result = {
                ...result,
                vars: varsConfig
            }
        }
        return result;
    }

    protected async getSourceConfig(sourceRepo: GitRepoInterface, type: string): Promise<Record<string, string>> {
        let sourceRepoConfigPath;
        if (!this.baseCommand.options['source-directory']) {
            const cloneStep = new InitCloneRepo(this.baseCommand.cwd(), sourceRepo, this.logger);
            await cloneStep.run();
            sourceRepoConfigPath = `${cloneStep.getTarget()}/${Me.CONFIG_FILE}`;
        } else {
            const sourceDir      = this.baseCommand.options['source-directory'];
            sourceRepoConfigPath = `${sourceDir}/${Me.CONFIG_FILE}`;
        }

        const sourceRepoConfig: PlagiarizeMeYamlConfigInterface
                  = yaml.safeLoad(fs.readFileSync(sourceRepoConfigPath, 'utf-8')) as PlagiarizeMeYamlConfigInterface;

        return sourceRepoConfig.replace[type];
    }

    private async _getStringConfig(sourceConfig: Record<string, string>): Promise<any> {

        const properties = [];
        // tslint:disable-next-line:forin
        for (const key in sourceConfig) {

            // noinspection JSUnfilteredForInLoop
            properties.push({
                name:     key,
                required: true,
                default:  sourceConfig[key],
            });
        }

        return new Promise<any>((resolve, reject) => {
            prompt.start();

            prompt.get(properties, (err, result) => {
                if (err) {
                    reject(err);
                }

                const finalResult = {};
                for(const key in result){
                    finalResult[key] = StringUtil.getAsProperScalar(result[key]);
                }

                resolve(finalResult);
            });
        });
    }

    public setRepo(repo: { url: string }){
        this._repo = repo;
    }

    public setSourceDirectory(sourceDirectory: string){
        this.baseCommand.options['source-directory'] = sourceDirectory;
    }

    private async _getRepo(): Promise<{ url: string }> {

        if(this._repo){
            return this._repo;
        }

        let urlDefault = path.basename(this.baseCommand.cwd());
        if (this.baseCommand.options['source-directory']) {
            urlDefault = this.baseCommand.options['source-directory']
        }

        this.logger.log(`Please enter git url and branch/tag of the project that contains the ${Me.CONFIG_FILE}. Can also be a local repository`);

        return new Promise<any>((resolve, reject) => {
            prompt.start();

            const properties = [
                {
                    name:     'url',
                    required: false,
                    default:  urlDefault,
                },
                {
                    name:     'checkout',
                    required: false,
                    default:  'master',
                },
            ];

            prompt.get(properties, (err, result) => {
                if (err) {
                    reject(err);
                }

                resolve({
                    url: result.url.trim(),
                    checkout: result.checkout.trim(),
                });
            });
        });
    }

    private _validatePlagiarizeNotExists() {
        const cwd = this.baseCommand.cwd();
        if (fs.existsSync(`${cwd}/${Init.CONFIG_FILE}`)) {
            throw new Error(`${cwd}/${Init.CONFIG_FILE} already exists. Please update the file manually or run '${Main.NAME} sync'`);
        }
    }

    private async _ensureWorkingDirectoryIsGitRepo() {
        const cwd = this.baseCommand.cwd();
        if (!fs.existsSync(`${cwd}/.git`)) {
            const git = new Git({
                dir: cwd
            });
            await git.init();
        }
    }

    public description(): string {
        return `Creates ${Init.CONFIG_FILE} file and initializes keys in source repository's ${Me.CONFIG_FILE} for configuration replace.strings`;
    }

    public name() {
        return this.constructor.name.toLowerCase();
    }

    public help() {
        this.baseCommand.help();
    }
}