import { StepInterface } from '../../step.interface';
import {FileIfInterface, PlagiarizeConfigInterface, PlagiarizeMeYamlConfigInterface} from '../../plagiarize-me-yaml-config.interface';
import * as path from 'path';
import * as glob from 'glob-promise';
import {clean, copy} from '../file';
import { ReplaceUtil } from '../../replace.util';
import { Filter } from '../filter';
import { Logger } from '../../logger';
import * as fs from 'fs';
import * as mkdirp from 'mkdirp';
import {StringUtil} from '../../util/string.util';

export class CopyReplace implements StepInterface {

  protected readonly replaceUtil: ReplaceUtil;
  protected replaceType = 'overwriting';

  constructor(
    protected readonly logger: Logger,
    protected readonly cwd: string,
    private readonly config: PlagiarizeConfigInterface,
    protected readonly sourceConfig: PlagiarizeMeYamlConfigInterface,
    protected readonly sourceDirectory: string,
  ) {
    this.replaceUtil = new ReplaceUtil(logger, cwd, sourceConfig.replace.strings, config.strings, config.vars || {}, sourceConfig.replace.values)
  }

  public async run() {
    this.logger.log('Started replacing files');
    await this._run(this.sourceConfig.replace?.files || []);
    await this._runIf(this.sourceConfig.replace?.filesIf || []);
    this.logger.log('Finished replacing files');
  }

  private async _runIf(filesIf: FileIfInterface[]){
    const files = [];
    for(const config of filesIf){
      if(StringUtil.getAsProperScalar(this.replaceUtil.replaceConfig(config.condition))){
        files.push(config.path);
      }
    }
    await this._run(files);
  }

  protected async _run(files) {
    let promises = [];

    for(const file of files){

      if(!file.includes('*')){
        promises.push(this._copyFile(file));
      }else{
        promises = promises.concat(await this._replaceGlobPromises(file));
      }
    }

    await Promise.all(promises).catch((error)=> {
      this.logger.error(error);
      this.logger.debug(error.stack);
      process.exit(1);
    });
  }

  protected async _copyFile(rawFile){
    const file = this.replaceUtil.config(rawFile);
    const to = this.replaceUtil.replaceConfig(rawFile);

    const fromFile = path.resolve(`${this.sourceDirectory}/${file}`);
    const toFile = path.resolve(`${this.cwd}/${to}`);

    if(!fs.existsSync(path.dirname(toFile))) {
      await mkdirp(path.dirname(toFile));
    }

    this.logger.debug(`Starting ${this.replaceType} ${toFile} with ${fromFile}`);
    await this.copy(fromFile, toFile);
    this.logger.debug(`Finished ${this.replaceType} ${toFile} with ${fromFile}`);
  }

  protected async copy(fromFile: string, toFile: string) {
    const removedSourceDirectoryFromFile = fromFile.replace(this.sourceDirectory, '');
    const outputFile = await clean(this.sourceDirectory, removedSourceDirectoryFromFile, this.replaceUtil);
    await copy(outputFile, toFile,this.logger, new Filter(this.replaceUtil), true);
  }

  private async _replaceGlobPromises(pattern: string) {
    const files = await glob.promise(path.resolve(`${this.cwd}/${pattern}`));
    const promises = [];
    for(const file of files) {
      promises.push(this._copyFile(file));
    }
    return promises;
  }

}