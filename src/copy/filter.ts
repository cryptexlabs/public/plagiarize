import { Transform } from 'stream';
import { ReplaceUtil } from '../replace.util';

export class Filter extends Transform {

  constructor(private readonly replaceUtil: ReplaceUtil) {
    super({
      readableObjectMode: true,
      writableObjectMode: true
    })
  }

  _transform(chunk, encoding, next) {
    const replacedBuffer = Buffer.from(this.replaceUtil.replace(chunk.toString()));
    next(null, replacedBuffer);
  }
}