import { ReplaceUtil } from '../../replace.util';
import * as fs from 'fs';
import * as jq from 'node-jq';
import { MergePatternStepInterface } from './merge-pattern-step-interface';
import {Logger} from '../../logger';

export class CopyMergeJq implements MergePatternStepInterface {

  constructor(
    private readonly replaceUtil: ReplaceUtil,
    private readonly logger: Logger,
    private readonly sourceDirectory: string,
    private readonly targetDirectory: string,
    private readonly file,
    private readonly tempFilePath: string
  ) {

  }

  public async create(pattern: string, createPatterns: string[]): Promise<any> {
    return this._run(pattern, false);
  }

  public replace(pattern: string, createPatterns: string[]): Promise<any> {
    return this._run(pattern, true);
  }

  public async set(pattern: string, value: string, createPatterns: string[]): Promise<any> {

    const to = this.replaceUtil.replace(this.file);
    let targetValue = this.replaceUtil.replace(value);

    let dynamicValue;
    let currentTargetValue;
    const toFilePath = `${this.targetDirectory}/${to}`;
    if(fs.existsSync(toFilePath)) {
      currentTargetValue = await this._getResult(toFilePath, pattern);
    }

    dynamicValue = this.replaceUtil.replaceDynamic(value, currentTargetValue, toFilePath);

    if(targetValue === 'null'){
      targetValue = '';
    }

    return this._run(pattern, true, dynamicValue || targetValue);
  }

  public async skip(pattern: string, createPatterns: string[]): Promise<any> {
    const value = await this._getResult(this.targetDirectory, pattern);
    return this._run(pattern, true, value);
  }

  public async _run(pattern: string, overwrite: boolean, setValue?: any): Promise<void> {
    const sourceFile = `${this.sourceDirectory}/${this.file}`

    if(!overwrite){
      const targetValue = await this._getResult(this.tempFilePath, pattern);
      if(targetValue !== 'null') {
        return;
      }
    }

    let writeValue = setValue;
    if(!setValue){
      const sourceValue = await this._getResult(sourceFile, pattern);

      writeValue = this.replaceUtil.replace(sourceValue.toString());
    }

    const replacePattern = `${pattern} = ${writeValue.toString()}`;

    const result = await this._getResult(this.tempFilePath, replacePattern);

    const writeStream = fs.createWriteStream(this.tempFilePath);

    writeStream.write(result);

    writeStream.close();

    await new Promise<void>((resolve) => {
      writeStream.on('close', resolve);
    });
  }

  private async _getResult(path: string, pattern: string): Promise<any> {
    if(fs.existsSync(path)){
      try{
        return await jq.run(`${pattern}`, path);
      }catch (e){
        this.logger.error(e);
        throw new Error(`Jq error with pattern '${pattern}' on path '${path}'`);
      }

    }else{
      return '';
    }
  }
}
