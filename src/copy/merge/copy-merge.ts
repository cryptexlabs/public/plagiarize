import { StepInterface } from '../../step.interface';
import { DataMergeType, PlagiarizeConfigInterface, PlagiarizeMeYamlConfigInterface } from '../../plagiarize-me-yaml-config.interface';
import { ReplaceUtil } from '../../replace.util';
import { CopyMergeData } from './copy-merge-data';
import { CopyMergeGit } from './copy-merge-git';
import { Logger } from '../../logger';

export class CopyMerge implements StepInterface {

  protected readonly replaceUtil: ReplaceUtil;

  constructor(
    private readonly logger: Logger,
    protected readonly cwd: string,
    private readonly config: PlagiarizeConfigInterface,
    protected readonly sourceConfig: PlagiarizeMeYamlConfigInterface,
    protected readonly sourceDirectory: string,
    private readonly outputDirectory: string
  ) {
    this.replaceUtil = new ReplaceUtil(logger, cwd, sourceConfig.replace.strings, config.strings, config.vars || {}, sourceConfig.replace.values);
  }

  public async run() {
    this.logger.log('Started merging files');

    await this.replaceUtil.initializeDynamicValues(this.sourceConfig);

    const promises = [];
    for (const replaceItem of this.sourceConfig.merge || []) {
      // @ts-ignore
      if (Object.values(DataMergeType).includes(replaceItem.type)) {
        promises.push(new CopyMergeData(this.logger, this.cwd, this.replaceUtil, this.sourceDirectory, replaceItem, this.outputDirectory).run());
      } else {
        promises.push(new CopyMergeGit(this.logger, this.cwd, this.replaceUtil, this.sourceDirectory, replaceItem.path).run());
      }
    }
    await Promise.all(promises);

    this.logger.log('Finished merging files');
  }

}