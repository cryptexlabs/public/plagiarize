import * as fs from 'fs';
import { exec } from 'child-process-promise';
import { ReplaceUtil } from '../../replace.util';
import { MergePatternStepInterface } from './merge-pattern-step-interface';
import {Logger} from '../../logger';
import * as jsYaml from 'js-yaml';
import {safeLoad} from 'js-yaml';

export class CopyMergeYq implements MergePatternStepInterface {

  constructor(
    private readonly replaceUtil: ReplaceUtil,
    private readonly logger: Logger,
    private readonly sourceDirectory: string,
    private readonly targetDirectory: string,
    private readonly file,
    private readonly tempFilePath: string,
  ) {

  }

  public async create(pattern: string, createPatterns: string[]): Promise<any> {
    const to = this.replaceUtil.replace(this.file);
    const value = await this._getResult(`${this.targetDirectory}/${to}`, pattern);
    if(!value){
      return this._run(pattern);
    }
  }

  public replace(pattern: string, createPatterns: string[]): Promise<any> {
    return this._run(pattern);
  }

  public async set(pattern: string, value: string, createPatterns: string[]): Promise<any> {

    const to = this.replaceUtil.replace(this.file);
    let targetValue = this.replaceUtil.replace(value);

    let dynamicValue;
    let currentTargetValue;
    const toFilePath = `${this.targetDirectory}/${to}`;
    if(fs.existsSync(toFilePath)) {
      currentTargetValue = await this._getResult(toFilePath, pattern);
    }

    dynamicValue = this.replaceUtil.replaceDynamic(value, currentTargetValue, toFilePath);

    if(targetValue === 'null' || targetValue === null){
      targetValue = '';
    }

    return this._run(pattern, dynamicValue || targetValue);
  }

  public async skip(pattern: string, createPatterns: string[]): Promise<any> {
    const to = this.replaceUtil.replace(this.file);
    const value = await this._getResult(`${this.targetDirectory}/${to}`, pattern);
    return this._run(pattern, value, true);
  }

  private async _run(pattern: string, setValue?: any, allowEmptyValue = false): Promise<any> {
    const sourceFile = `${this.sourceDirectory}/${this.file}`;

    let writeValue = setValue;
    if(!setValue && !allowEmptyValue){
      writeValue = this.replaceUtil.replace(await this._getResult(sourceFile, pattern));
    }

    if(setValue || !allowEmptyValue){

      const sourceResultPath        = `${this.tempFilePath}-query`;
      const sourceResultWriteStream = fs.createWriteStream(sourceResultPath);

      sourceResultWriteStream.write(writeValue);
      sourceResultWriteStream.close();

      await new Promise<void>((resolve) => {
        sourceResultWriteStream.on('close', resolve);
      });

      const currentValue = await this._getResult(this.tempFilePath, pattern);

      // If a current value exists then we can update it
      if(!currentValue) {
        // get parent object
        const parentPatternParts = pattern.split('.');
        const lastMatcher = parentPatternParts.pop();
        const parentPattern = parentPatternParts.join('.');

        const parentObject = jsYaml.safeLoad(await this._getResult(this.tempFilePath, parentPattern));
        let updatePattern;
        if(Array.isArray(parentObject)){
          updatePattern = `${parentPattern}[+]`
        }else{
          const updateLastMatcher =  lastMatcher
              .replace('(', '')
              .replace(')', '')
              .replace('=', '')
              .replace('!', '')
          updatePattern = `${parentPattern}.${updateLastMatcher}`
        }
        await exec(`yq w -i ${this.tempFilePath} '${updatePattern}' -f ${sourceResultPath}`);
      }

      try {
        await exec(`yq w -i ${this.tempFilePath} '${pattern}' -f ${sourceResultPath}`);
      }catch (e){
        throw e;
      }

      // fs.unlinkSync(sourceResultPath);
    }else{
      try {
        await exec(`yq w -i ${this.tempFilePath} '${pattern}' ''`);
      }catch (e){
        throw e;
      }
    }
  }

  private async _getResult(path: string, pattern: string): Promise<string> {
    if(fs.existsSync(path)){
      const execResult = await exec(`yq r ${path} '${pattern}'`);
      return execResult.stdout.trim();
    }else{
      return '';
    }
  }
}