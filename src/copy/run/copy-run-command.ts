import {Logger} from '../../logger';
import {EventEmitter} from 'events';

// tslint:disable-next-line:no-var-requires
const util = require('util');
// tslint:disable-next-line:no-var-requires
const exec = util.promisify(require('child_process').exec);

export class CopyRunCommand extends EventEmitter {

    constructor(
        private readonly logger: Logger,
        private readonly cwd: string,
        private readonly command: string,
        private readonly onerror: string,
    ) {
        super();
    }

    public async run(): Promise<void>{

        this.logger.debug(`Started command '${this.command}'`)
        try{
            await this._execWithPipedOutput(this.command);
        }catch (e){
            this.logger.error(e);
            if(this.onerror){
                this.logger.debug(`Started onerror command '${this.onerror}'`)

                await this._execWithPipedOutput(this.onerror);

                this.logger.debug(`Finished onerror command '${this.onerror}'`)
            }
            this.logger.debug(e.stack);
            process.exit(1);
        }

        this.logger.debug(`Finished command '${this.command}'`)
    }

    private async _execWithPipedOutput(command: string): Promise<void>{
        const result = await exec(command, { shell: true, cwd: this.cwd });
        const { stdout, stderr } = result;
        this.logger.debug(stdout);

        if(stderr){
          this.logger.warn(stderr);
        }
    }
}