export interface CopyRunCommandInterface {
    run(): Promise<void>
}