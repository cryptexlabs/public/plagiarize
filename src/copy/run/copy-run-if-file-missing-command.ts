import {CopyRunCommandInterface} from './copy-run-command.interface';
import * as fs from 'fs';
import {Logger} from '../../logger';
import {ReplaceUtil} from '../../replace.util';
import {CopyRunCommand} from './copy-run-command';
import {RunIfMissing, RunIfMissingOptions, RunInterface} from '../../plagiarize-me-yaml-config.interface';

export class CopyRunIfFileMissingCommand implements CopyRunCommandInterface {

    constructor(
        private readonly logger: Logger,
        private readonly cwd: string,
        private readonly replaceUtil: ReplaceUtil,
        private readonly config: RunInterface<RunIfMissing>,
    ) {
    }

    async run(): Promise<void> {

        const options = this.config.when.options as RunIfMissingOptions;

        const checkPath = this.replaceUtil.replaceConfig(options.path);

        if(!fs.existsSync(`${this.cwd}/${checkPath}`)){
            const command = this.replaceUtil.replaceConfig(this.config.command);
            const onerror = this.config.onerror ? this.replaceUtil.replaceConfig(this.config.onerror) : undefined;
            await new CopyRunCommand(this.logger, this.cwd, command, onerror).run();
        }
    }
}