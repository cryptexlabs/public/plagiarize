import { StepInterface } from '../../step.interface';
import { PlagiarizeConfigYamlInterface } from '../../plagiarize-config-yaml.interface';
import {
  MergeItemInterface,
  PlagiarizeConfigInterface,
  PlagiarizeMeYamlConfigInterface,
  ReplaceSourceInterface,
  TemplateItemInterface,
} from '../../plagiarize-me-yaml-config.interface';
import { ReplaceUtil } from '../../replace.util';
import { CopyReplace } from '../replace/copy-replace';
import { CopyMerge } from '../merge/copy-merge';
import { Logger } from '../../logger';
import { CopyCreate } from '../create/copy-create';

export class CopyTemplate implements StepInterface {

  protected readonly replaceUtil: ReplaceUtil;

  constructor(
    private readonly logger: Logger,
    private readonly cwd: string,
    private readonly config: PlagiarizeConfigYamlInterface,
    private readonly sourceConfig: PlagiarizeMeYamlConfigInterface,
    protected readonly sourceDirectory: string,
    private readonly outputDirectory: string
  ) {
    this.replaceUtil = new ReplaceUtil(logger, cwd, sourceConfig.replace.strings, config.strings, config.vars || {}, sourceConfig.replace.values);
  }

  public async run() {
    this.logger.log('Started templating files');

    await this.replaceUtil.initializeDynamicValues(this.sourceConfig);

    for(const template of this.sourceConfig.templates || []){
      await this._run(template);
    }

    this.logger.log('Finished templating files');
  }

  private async _run(template: TemplateItemInterface): Promise<void>{

    const templateConfig: PlagiarizeConfigInterface = {
      strings: template.replace.strings,
      vars: template.replace.vars || {}
    }
    await this._runConfig(
      templateConfig,
      template.replace,
      template.merge,
      template.create
    )

    for(const config of template.configs || []){
      await this._runConfig(
          {
            strings: config.strings,
            vars: {
              ...(template.replace.vars  || {}),
              ...(config.vars || {}),
            }
          },
        template.replace,
        template.merge,
        template.create
      );
    }

  }

  private async _runConfig(
    templateTargetConfig: PlagiarizeConfigInterface,
    templateSourceReplaceConfig: ReplaceSourceInterface,
    templateSourceMergeItems: MergeItemInterface[],
    templateSourceCreate
  ) {

    const config: PlagiarizeConfigInterface = {
      strings: {
        ...this.config.strings,
        ...templateTargetConfig.strings,
      },
      vars: {
        ...(this.config.vars  || {}),
        ...(templateTargetConfig.vars || {})
      }
    }

    const newStrings = {
      ...templateSourceReplaceConfig.strings
    };
    for(const key in this.sourceConfig.replace.strings) {
      newStrings[key] = templateSourceReplaceConfig.strings[key] ? templateSourceReplaceConfig.strings[key] : this.sourceConfig.replace.strings[key]
    }

    const newVars = {
      ...(templateSourceReplaceConfig.vars || {})
    };
    for(const key in this.sourceConfig.replace.vars || {}){
      newVars[key] = templateSourceReplaceConfig.vars[key] ? templateSourceReplaceConfig.vars[key] : this.sourceConfig.replace.vars[key];
    }

    const sourceConfig: PlagiarizeMeYamlConfigInterface = {

      repo: { url: null},
      replace: {
        strings: newStrings,
        vars: newVars,
        values: {
          ...templateSourceReplaceConfig.values,
          ...(this.sourceConfig.replace.values ? this.sourceConfig.replace.values : {})
        },
        files: templateSourceReplaceConfig.files ? templateSourceReplaceConfig.files : [],
      },
      merge: templateSourceMergeItems,
      create: templateSourceCreate
    }

    const steps = [
      new CopyReplace(this.logger, this.cwd, config, sourceConfig, this.sourceDirectory),
      new CopyCreate(this.logger, this.cwd, config, sourceConfig, this.sourceDirectory),
      new CopyMerge(this.logger, this.cwd, config, sourceConfig, this.sourceDirectory, this.outputDirectory)
    ];

    const promises = [];

    for(const step of steps){
      promises.push(step.run())
    }

    await Promise.all(steps);
  }

}