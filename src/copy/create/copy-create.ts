import { StepInterface } from '../../step.interface';
import { clean, copy, create } from '../file';
import { CopyReplace } from '../replace/copy-replace';
import { Filter } from '../filter';
import { PlagiarizeConfigInterface, PlagiarizeMeYamlConfigInterface } from '../../plagiarize-me-yaml-config.interface';
import * as fs from 'fs';
import { Logger } from '../../logger';

export class CopyCreate extends CopyReplace implements StepInterface {

  constructor(
    logger: Logger,
    cwd: string,
    config: PlagiarizeConfigInterface,
    sourceConfig: PlagiarizeMeYamlConfigInterface,
    sourceDirectory: string
  ) {
    super(logger, cwd, config, sourceConfig, sourceDirectory);
    this.replaceType = 'creating'
  }

  public async run() {
    this.logger.log('Started creating files');

    const files = this.sourceConfig.create?.files || [];
    const cleanFiles = this.sourceConfig.create?.andClean || [];
    const noCleanFiles = files.filter(file => !cleanFiles.includes(file))

    await this._run(noCleanFiles);
    await this._cleanFiles();
    this.logger.log('Finished creating files');
  }

  protected async copy(fromFile: string, toFile: string) {
    this.logger.debug(`CopyCreate: copy ${fromFile} to ${toFile}`)
    await create(fromFile, toFile,this.logger, new Filter(this.replaceUtil));
  }

  private async _cleanFiles() {
    for(const file of this.sourceConfig.create?.andClean || []){
      if(!fs.existsSync(`${this.cwd}/${file}`)) {
        const outputFile = await clean(this.sourceDirectory, file, this.replaceUtil);
        await copy(outputFile, `${this.cwd}/${file}`,this.logger, new Filter(this.replaceUtil), true);
      }
    }
  }

}