import { GitPostPushHookInterface } from '../git-post-push-hook.interface';
import { GitlabUtil } from '../../../../util/gitlab.util';
import { Gitlab } from '@gitbeaker/node';
import axios from 'axios';
import { GitRepoInterface, PushGitInterface, PushGitPostPushHook } from '../../../../plagiarize-me-yaml-config.interface';
import * as urlencode from 'urlencode';
import { Logger } from '../../../../logger';
import { CreateMergeRequestOptions } from '@gitbeaker/core/dist/types/services/MergeRequests';
import {ReplaceUtil} from '../../../../replace.util';

export class GitlabGitPostPushHookCreateMergeRequest implements GitPostPushHookInterface {

  constructor(
    private readonly logger: Logger,
    private readonly replaceUtil: ReplaceUtil,
    private readonly hook: PushGitPostPushHook,
    private readonly repo: GitRepoInterface,
    private readonly gitConfig: PushGitInterface
  ) {
  }

  public async run(): Promise<void> {
    if(!process.env[this.hook.envVars.apiKey]) {
      throw new Error(`Error: Environment variable ${this.hook.envVars.apiKey} is not set`);
    }

    const apiKey = process.env[this.hook.envVars.apiKey];

    const repoUrl = this.replaceUtil.replaceConfig(this.repo.url);

    const projectPath = await GitlabUtil.getProjectPath(repoUrl);

    const api = new Gitlab({
      token: apiKey,
    });

    const urlEncodedProjectPath = urlencode(projectPath);

    this.logger.debug(`Looking up project https://gitlab.com/api/v4/projects/${urlEncodedProjectPath}`)
    const response = await axios.get(
      `https://gitlab.com/api/v4/projects/${urlEncodedProjectPath}`,
      {
        headers: {
          'Private-Token': apiKey
        }
      }
    );

    if(!(response.status >= 200 || response.status < 400)){
      this.logger.error(`Gitlab: Error getting project: ${projectPath} to create merge request`);
      this.logger.error(response.data);
      return;
    }

    const projectId = response.data.id;

    this.logger.log(`Creating merge request for project ${projectId}`)

    const commitMessage = this.replaceUtil.replaceConfig(this.gitConfig.commit.message);

    const sourceBranch = this.gitConfig.branch || 'master';
    this.logger.debug(`source branch: ${sourceBranch}`);
    this.logger.debug(`target branch: ${this.hook.targetBranch}`);
    this.logger.debug(`merge request name: '${commitMessage}'`);

    if(sourceBranch === this.hook.targetBranch){
      throw new Error(`Source branch '${sourceBranch}' cannot be the same as target branch '${this.hook.targetBranch}'`)
    }

    interface GitlabMergeRequestInterface {
      title: string,
      state: string
    }

    const mergeRequests: GitlabMergeRequestInterface[] = await api.MergeRequests.all({projectId}) as GitlabMergeRequestInterface[];

    for(const mergeRequest of mergeRequests){
      if(mergeRequest.title === commitMessage && mergeRequest.state === 'opened'){
        this.logger.debug(`Merge request with name '${commitMessage}' already exists. Skipping merge request creation`);
        return;
      }
    }

    const options: CreateMergeRequestOptions = {
      removeSourceBranch: this.hook.removeSourceBranch || false
    }

    try {
      await api.MergeRequests.create(
        projectId,
        sourceBranch,
        this.hook.targetBranch,
        this.gitConfig.commit.message,
        options
      );
    }catch (e){
      if(typeof e === 'object' && e.constructor.name === 'HTTPError') {
        if(e.response.statusCode === 409){
          this.logger.debug('Merge request already exists for that branch and with that name');
        }else{
          this.logger.warn(`Warning: ${e}`);
        }
      }else{
        this.logger.warn(`Warning: ${e}`);
      }
    }

  }
}