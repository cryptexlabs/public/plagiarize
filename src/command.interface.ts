export interface OptionsDefinition {
  name: string;
  alias?: string;
  description: string
  default?: any | boolean;
  // tslint:disable-next-line:ban-types
  type: Function;
}

export interface CommandInterface {
  run(): Promise<any>;
  help();
  description(): string;
  name(): string;
  initializeOptions(optionsDefinitions?: OptionsDefinition[]): Record<string, any>;
  setCWD(cwd: string);
}