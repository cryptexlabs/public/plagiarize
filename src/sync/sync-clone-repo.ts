import { StepInterface } from '../step.interface';
import * as pullOrClone from 'git-pull-or-clone';
import * as path from 'path';
import { Main } from '../main';

export class SyncCloneRepo implements StepInterface {

  private readonly cwdBaseName: string;

  constructor(private readonly cwd: string, private readonly gitRepo) {
    this.cwdBaseName = path.basename(cwd);
  }

  async run() {

    await new Promise<any>((resolve, reject) => {
      pullOrClone(this.gitRepo, this.getTarget(), (error) => {
        if(!error){
          resolve();
        }else{
          reject(error);
        }
      });
    })
  }

  public getTarget() {
    return `/tmp/${Main.NAME}-clone-for-${this.cwdBaseName}`
  }

}