export class StringUtil {
    private constructor() {
    }

    public static getAsProperScalar (str: string): any {
        let typeCastedValue = str as any;
        if(typeCastedValue === 'true'){
          typeCastedValue = true;
        }
        if(typeCastedValue === 'false'){
          typeCastedValue = false;
        }
        // tslint:disable-next-line:triple-equals
        if(typeof typeCastedValue === 'string'
            && !Number.isNaN(typeCastedValue)
            && parseInt(typeCastedValue, 10) === parseFloat(typeCastedValue)){
          typeCastedValue = parseInt(typeCastedValue, 10);
        }

        // tslint:disable-next-line:triple-equals
        const looseTypeComparisonIsSame = parseFloat(typeCastedValue) == typeCastedValue;

        // Convert to double
        if(typeof typeCastedValue === 'string'
            && !Number.isNaN(typeCastedValue)
            && parseInt(typeCastedValue, 10) !== parseFloat(typeCastedValue)
            && looseTypeComparisonIsSame
        ){
          typeCastedValue = parseInt(typeCastedValue, 10);
        }
        return typeCastedValue
    }
}