export class GitlabUtil {

  public static async getProjectPath(remoteUrl: string) {

    if (remoteUrl.includes('https://')) {
      return this._getProjectPathFromHttps(remoteUrl);
    } else {
      return this._getProjectPathFromSSH(remoteUrl);
    }
  }

  private static _getProjectPathFromHttps(remoteUrl: string): string {
    // TODO
    return '';
  }

  private static _getProjectPathFromSSH(remoteUrl: string): string {
    const gitParts     = remoteUrl.split('@');
    const urlParts     = gitParts[1].split(':');
    const domain       = urlParts[0];
    const projectParts = urlParts[1].split('.git');
    return projectParts[0];
  }

}