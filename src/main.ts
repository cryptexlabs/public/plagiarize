import { CommandInterface } from './command.interface';
import { Init } from './init/init';
import { Copy } from './copy/copy';
import { Me } from './me/me';
import { Push } from './push/push';
import { Sync } from './sync/sync';
import * as commandLineArgs from 'command-line-args';
import { Logger } from './logger';
import {Template} from './template/template';

// tslint:disable-next-line:no-var-requires
const packageJson = require('../package.json')

export class Main {

  public static readonly NAME = 'plagiarize'

  commands: any;
  debug: boolean
  logger: Logger;

  constructor() {
    this.commands = {
      [Init.name.toLowerCase()]: Init,
      [Copy.name.toLowerCase()]: Copy,
      [Me.name.toLowerCase()]:   Me,
      [Push.name.toLowerCase()]: Push,
      [Sync.name.toLowerCase()]: Sync,
      [Template.name.toLowerCase()]: Template,
    };
    this.debug = true;
    this.logger = new Logger();
  }

  public async run() {

    let command = { run: () => {}, initializeOptions: (): Record<string, any> => { return {}} }
    try {

      const definitions = [
        { name: 'command', defaultOption: true },
        { name: 'help', alias: 'h', type: Boolean, default: false },
        { name: 'debug', type: Boolean, default: false },
        { name: 'version', alias: 'v', type: Boolean, default: false },
      ];

      const options = commandLineArgs(definitions, { stopAtFirstUnknown: true });

      const inputCommand = options.command;

      this.debug = options.debug;

      if(options.help && !inputCommand){
        this.help()
        process.exit(0);
      }

      if(options.version){
        this.version()
        process.exit(0);
      }

      if (!this.commands[inputCommand]) {
        this.help();
        process.exit(1);
      }

      const clazz = this.commands[inputCommand];

      command = new clazz(this.logger) as CommandInterface;
      const subCommandOptions = command.initializeOptions();
      this.debug = subCommandOptions.debug;
      this.logger.debugEnabled = subCommandOptions.debug;
      await command.run();
    }catch (e) {
      if(this.debug){
        this.logger.error(e ? e.stack : 'Unknown Error');
      }else{
        this.logger.error(e ? e.message : 'Unknown Error');
      }
      process.exit(1);
    }

  }

  public version() {
    this.logger.log(packageJson.version);
  }

  public help() {

    const asciiArt = '  _____  _             _            _         \n' +
      ' |  __ \\| |           (_)          (_)        \n' +
      ' | |__) | | __ _  __ _ _  __ _ _ __ _ _______ \n' +
      ' |  ___/| |/ _` |/ _` | |/ _` | \'__| |_  / _ \\\n' +
      ' | |    | | (_| | (_| | | (_| | |  | |/ /  __/\n' +
      ' |_|    |_|\\__,_|\\__, |_|\\__,_|_|  |_/___\\___|\n' +
      '                  __/ |                       \n' +
      '                 |___/                        '


    this.logger.log(asciiArt);
    this.logger.log(`${this.description()}\n`);
    this.logger.log('Usage:')
    this.logger.log(`  ${this.name()} [command]\n`)
    this.logger.log('Available Commands:');

    let maxLengthKey = 0;
    for(const key in this.commands){
      if(key.length > maxLengthKey){
        maxLengthKey = key.length;
      }
    }

    // tslint:disable-next-line:forin
    for(const key in this.commands){
      // noinspection JSUnfilteredForInLoop
      const clazz = this.commands[key];
      const command = new clazz() as CommandInterface;

      this.logger.log(`  ${key.padStart(maxLengthKey,' ')}  ${command.description()}`);
    }
    this.logger.log(`\nUse "${this.name()} [command] --help" for more information about a command`);
  }

  public description(): string {
    return 'Clone, search, update and rename strings & directories for any git project';
  }

  public name() {
    return Main.NAME;
  }
}