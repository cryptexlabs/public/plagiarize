import { CommandInterface, OptionsDefinition } from './command.interface';
import * as commandLineArgs from 'command-line-args';
import * as path from 'path';
import { Main } from './main';
import { Logger } from './logger';
import * as os from 'os';

export class BaseCommand {

  public options: Record<string, any>;
  public optionsDefinitions: OptionsDefinition[];
  private _cwd: string;

  public constructor(private readonly logger: Logger, private readonly subCommand: CommandInterface) {
    this.options = {};
  }

  public initializeOptions(optionsDefinitions: OptionsDefinition[]) : Record<string, any> {
    const mainDefinitions = [
      { name: 'command', defaultOption: true }
    ];
    const mainOptions = commandLineArgs(mainDefinitions, { stopAtFirstUnknown: true });
    const argv = mainOptions._unknown || [];

    this.optionsDefinitions = optionsDefinitions.concat([
      { name: 'help', alias: 'h', type: Boolean, default: false, description: 'Show help text'},
      { name: 'directory', alias: 'd', type: String, description: 'The directory to use for current working directory' },
      { name: 'debug', type: Boolean, default: false, description: 'Enable debugging. Turns on stack trace output for errors' },
    ]);

    try{
      this.options = commandLineArgs(this.optionsDefinitions, { argv });
      
      if(this.options.directory){
        this._cwd = this.options.directory;
      }
    }catch (e){
      throw e;
    }
    return this.options;
  }

  public run(): any {
    if(this.options.help){
      this.help();
      process.exit(0);
    }
  }

  public setCWD(cwd: string){
    this._cwd = cwd;
  }

  public cwd(): string {
    const cwd = this._cwd || this.options.directory || process.cwd();
    const homePathResolved = cwd.replace('~', os.homedir());
    return path.resolve(homePathResolved);
  }

  public help() {
    this.logger.log(`\n${this.subCommand.description()}\n`);

    this.logger.log('Usage:')
    this.logger.log(`  ${Main.NAME} ${this.subCommand.name()}\n`)

    this.logger.log('Flags:');

    let maxNameLength = 0;
    for(const definition of this.optionsDefinitions){
      if(definition.name.length > maxNameLength){
        maxNameLength = definition.name.length;
      }
    }

    for(const definition of this.optionsDefinitions) {

      const alias = definition.alias ? (`-${definition.alias}`).padStart(2, ' ') : '  ';
      const aliasSection = definition.alias ? `  ${alias},` : ''.padStart(5, ' ');
      const name = definition.name.padEnd(maxNameLength, ' ');
      this.logger.log(`${aliasSection} --${name}   ${definition.description}`);
    }
  }
}