export interface GitRepoInterface {
  url: string;
  checkout?: string;
  defaultName?: string;
}

export interface MoveItemInterface {
  to: string;
  names: string[]
}

export interface MoveInterface {
  directories: MoveItemInterface[];
  files: MoveItemInterface[];
}

export interface ReplaceSourceInterface extends ReplaceTargetInterface {
  values?: Record<string, string>;
}

export interface FileIfInterface {
  condition: string;
  path: string
}

export interface ReplaceTargetInterface {
  strings: Record<string, string>;
  files?: string[];
  vars?: Record<string, string>;
  filesIf?: FileIfInterface[]
}

export interface CreateInterface {
  files: string[];
  andClean: string[];
}

export enum RunWhenType {
  ALWAYS = 'always',
  IF_MISSING = 'if-missing'
}

export interface RunWhen extends String {
  type: RunWhenType;
}

export interface RunIfMissingOptions {
  path: string
}

export interface RunIfMissing extends RunWhen {
  type: RunWhenType.IF_MISSING
  options: RunIfMissingOptions
}

export interface RunAlways extends RunWhen {
  type: RunWhenType.ALWAYS
  options: RunIfMissingOptions
}

export interface RunInterface<T extends RunWhen> {
  command: string;
  onerror?: string;
  when: T
  parallel?: boolean
}

export enum DataMergeType {
  YQ = 'yq',
  JQ = 'jq',
  DOT_ENV = 'env',
}

export declare type MergeType = DataMergeType | 'git';

export interface ReplaceIfInterface {
  condition: string;
  pattern: string;
}

export interface MergeItemInterface {
  type: MergeType;
  path: string;
  replace?: string[];
  replaceIf?: ReplaceIfInterface[];
  create?: string[];
  set?: Record<string, string>;
  skip?: string[];
  skipIfNotSameFileName?: string[];
}

export interface TemplateItemInterface {
  name: string;
  replace?: ReplaceTargetInterface;
  merge?: MergeItemInterface[];
  create?: CreateInterface;
  configs?: PlagiarizeConfigInterface[];
}

export interface PushRepoItemInterface {
  local?: string;
  remote: GitRepoInterface;
}

export interface PushGitCommitInterface {
  create: boolean;
  message: string;
}

export interface PushGitPostPushHookGitlabEnvVars {
  apiKey: string;
}

export declare type PushGitPostPushHookEnvVars = PushGitPostPushHookGitlabEnvVars;

export interface PushGitPostPushHook {
  type: string;
  api: string;
  envVars: PushGitPostPushHookEnvVars;
  remote: string;
  targetBranch: string;
  removeSourceBranch?: boolean;
}

export declare type PushGitHook = PushGitPostPushHook;

export interface PushGitInterface {
  repos: PushRepoItemInterface[];
  commit: PushGitCommitInterface;
  branch: string;
  push: boolean;
  remotes: string[];
  hooks: Record<string, PushGitHook[]>;
}

export interface PushItemInterface {
  git: PushGitInterface;
  instances?: Record<string, any>[];
  values?: Record<string, any>;
}

export interface PlagiarizeConfigInterface {
  strings: Record<string, string>;
  vars?: Record<string, string>;
  override?: PlagiarizeMeYamlConfigInterface
}

export interface PlagiarizeMeYamlConfigInterface {
  move?: MoveInterface;
  replace?: ReplaceSourceInterface;
  create?: CreateInterface;
  run?: RunInterface<RunWhen>[];
  merge?: MergeItemInterface[];
  templates?: TemplateItemInterface[];
  push?: PushItemInterface[];
  repo: GitRepoInterface;
}