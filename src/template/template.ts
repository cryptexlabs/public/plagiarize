import {CommandInterface, OptionsDefinition} from '../command.interface';
import {BaseCommand} from '../base-command';
import {Me} from '../me/me';
import {Logger} from '../logger';
import {CopyTemplate} from '../copy/template/copy-template';
import {PlagiarizeConfigYamlInterface} from '../plagiarize-config-yaml.interface';
import {PlagiarizeMeYamlConfigInterface} from '../plagiarize-me-yaml-config.interface';
import * as yaml from 'js-yaml';
import * as fs from 'fs';

export class Template implements CommandInterface {

    protected readonly baseCommand: BaseCommand;

    public constructor(protected readonly logger: Logger) {
        this.baseCommand = new BaseCommand(logger, this);
    }

    public initializeOptions(optionsDefinitions: OptionsDefinition[] = []): Record<string, any> {
        return this.baseCommand.initializeOptions(optionsDefinitions);
    }

    public setCWD(cwd: string) {
        this.baseCommand.setCWD(cwd);
    }

    public async run() {
        this.baseCommand.run();

        this.logger.debug(`Started loading  ${Me.CONFIG_FILE}`);
        const sourceConfig: PlagiarizeMeYamlConfigInterface
                  = yaml.safeLoad(fs.readFileSync(`${this.baseCommand.cwd()}/${Me.CONFIG_FILE}`, 'utf-8')) as PlagiarizeMeYamlConfigInterface;
        this.logger.debug(`Finished loading ${Me.CONFIG_FILE}`);

        const valuesConfig: PlagiarizeConfigYamlInterface = {
            repo: undefined,
            strings: sourceConfig.replace.strings,
            vars: sourceConfig.replace.vars || {}
        };

        const copyTemplate = new CopyTemplate(
            this.logger,
            this.baseCommand.cwd(),
            valuesConfig,
            sourceConfig,
            this.baseCommand.cwd(),
            this.baseCommand.cwd()
        );

        await copyTemplate.run();
    }

    public description(): string {
        return `Runs templates in ${Me.CONFIG_FILE} to generate files in same project containing ${Me.CONFIG_FILE}`;
    }

    public name() {
        return this.constructor.name.toLowerCase();
    }

    public help() {
        this.baseCommand.help();
    }
}